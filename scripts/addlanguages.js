// const ADDLANG = this.ADDLANG || {};
//
// class addLanguagesConfig extends FormApplication {
//     constructor() {
//         super();
//         this.data = ADDLANG;
//     }
//
//     static get defaultOptions() {
//         return mergeObject(super.defaultOptions, {
//             id: "addLanguagesConfig",
//             title: "Add Languages",
//             template: "modules/addlanguages/templates/addlanguages.html",
//             classes: ["sheet"],
//             width: 500,
//             height: "auto",
//             resizable: true
//         });
//     }
//
//     getData() {
//         let entries = {};
//
//         for (let e of game.journal.entities) {
//             entries[e.id] = e.name;
//         }
//
//         const formData = {
//             conditionmap: this.data.map,
//             systems: this.data.systemChoices,
//             system: this.data.system,
//             entries: entries
//         };
//
//         return formData;
//     }
//
//     /**
//      * Take the new map and write it back to settings, overwriting existing
//      * @param {Object} event
//      * @param {Object} formdata
//      */
//     _updateObject(event, formdata) {
//         //console.log(event,formdata);
//         let newLang = [];
//         let newMap = [];
//         //const system = CUBSidekick.getGadgetSetting(this.data.GADGET_NAME + "(" + this.data.SETTINGS_DESCRIPTORS.SystemNameN + ")");
//         //let oldMap = oldMapsSetting[system];
//         //let mergeMapsSetting = {};
//
//         //need to tighten these up to check for the existence of digits after the word
//         const conditionRegex = new RegExp("condition", "i");
//         const iconRegex = new RegExp("icon", "i");
//         const journalRegex = new RegExp("journal", "i");
//
//
//         //write it back to the relevant condition map
//         //@todo: maybe switch to a switch
//         for (let e in formdata) {
//             if (e.match(conditionRegex)) {
//                 conditions.push(formdata[e]);
//             } else if (e.match(iconRegex)) {
//                 icons.push(formdata[e]);
//             } else if (e.match(journalRegex)) {
//                 entries.push(formdata[e]);
//             }
//         }
//
//         for (let i = 0; i <= conditions.length - 1; i++) {
//             newMap.push([conditions[i], icons[i], entries[i]]);
//         }
//
//         CUBSidekick.setGadgetSetting(this.data.GADGET_NAME + "(" + this.data.SETTINGS_DESCRIPTORS.MapsN + ")" + "." + this.data.system, newMap);
//
//         //not sure what to do about this yet, probably nothing
//         console.assert(conditions.length === icons.length, "There are unmapped conditions");
//     }
//
//     activateListeners(html) {
//         super.activateListeners(html);
//         let newSystem;
//         const addRowButton = html.find("button[class='add-row']");
//         const removeRowButton = html.find("button[class='remove-row']");
//         const restoreDefaultsButton = html.find("button[class='restore-defaults']");
//
//         systemSelector.change(async ev => {
//             //ev.preventDefault();
//             //find the selected option
//             const selection = $(ev.target).find("option:selected");
//
//             //capture the value of the selected option
//             newSystem = selection.val();
//
//             //set the enhanced conditions system to the new value
//             await CUBSidekick.setGadgetSetting(this.data.GADGET_NAME + "(" + this.data.SETTINGS_DESCRIPTORS.SystemN + ")", newSystem);
//
//             //if there's no mapping for the newsystem, create one
//             if (!this.data.settings.maps[newSystem]) {
//                 const newMap = [];
//
//                 await CUBSidekick.setGadgetSetting(this.data.GADGET_NAME + "(" + this.data.SETTINGS_DESCRIPTORS.MapsN + ")" + "." + newSystem, newMap);
//             }
//
//             //rerender the form to get the correct condition mapping template
//             this.render(true);
//         });
//
//         addRowButton.click(async ev => {
//             ev.preventDefault();
//             CUB.enhancedConditions.settings.maps[this.data.system].push(["", ""]);
//             this.render(true);
//         });
//
//         removeRowButton.click(async ev => {
//             //console.log(ev);
//             const splitName = ev.currentTarget.name.split("-");
//             const row = splitName[splitName.length - 1];
//
//             //console.log("row", row);
//             ev.preventDefault();
//             CUB.enhancedConditions.settings.maps[this.data.system].splice(row, 1);
//             this.render(true);
//         });
//
//         iconPath.change(async ev => {
//             ev.preventDefault();
//             //console.log("change", ev, this);
//             const splitName = ev.target.name.split("-");
//             const row = splitName[splitName.length - 1];
//
//             //target the icon
//             let icon = $(this.form).find("img[name='icon-" + row);
//             icon.attr("src", ev.target.value);
//         });
//
//         restoreDefaultsButton.click(async ev => {
//             ev.preventDefault();
//             //console.log("restore defaults clicked", ev);
//             CUB.enhancedConditions.settings.maps[this.data.system] = CUB.enhancedConditions.DEFAULT_MAPS[this.data.system] ? CUB.enhancedConditions.DEFAULT_MAPS[this.data.system] : [];
//             this.render(true);
//         });
//     }
//
//
// }
//
//
class addLanguages {
  static onReady() {
    addLanguages.addNewLang();
  }
  // static onRenderSettings() {
  //   addLanguages.addMenu();
  // }
  // static addMenu() {
  //
  // }
  static addNewLang() {
    //you can manually add in languages by adding in the text
    //CONFIG.DND5E.languages [""] =""
    //and putting the language name in the two sets of quote marks
    CONFIG.DND5E.languages ["sahuagin"] = "Sahuagin"
  }
}

Hooks.on("ready", addLanguages.onReady)
// Hooks.on("renderSettings", addLanguages.onRenderSettings)

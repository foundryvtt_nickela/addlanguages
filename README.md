# addLanguages
Allows the user to add language checkboxes to the dnd5e system for Foundry VTT.
## Project goals
*  Add in a menu to the options tab that allows users to add languages from within Foundry.
